
package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import utilities.Car;

class CarTests {

	@Test
	 public void testSpeedValidation() {
		try {Car r =new Car(-10);
		fail("the constructor was supposed to throw an exception but did not");
		}
		catch(IllegalArgumentException e) {
		}
		catch(Exception e){
			fail("the constructor throws an excetion but it was the wrong type");
		}
	}
	@Test
	 public void testGetSpeed() {
		Car x=new Car(10);
		assertEquals(10,x.getSpeed());
	}
	
	@Test
	 public void testGetLocation() {
		Car x=new Car(10);
		assertEquals(0,x.getLocation());
		
		}
	
	@Test
	 public void testMoveRight() {
		Car x=new Car (150);
		x.moveRight();
		assertEquals(150,x.getLocation());
		x.moveRight();
		assertEquals(300,x.getLocation());
	}
	@Test
	 public void testMoveLeft() {
    Car x=new Car (150);
	x.moveRight();
	x.moveRight();
	x.moveLeft();
	assertEquals(150,x.getLocation());
	
}
	@Test
	void testAccelerate() {
		 Car x=new Car (150);
			x.accelerate();
			assertEquals(151,x.getSpeed());
			
	
		
	}
	
	@Test
	void testDecelerate() {
		 Car x=new Car (150);
			x.decelerate();
			assertEquals(149,x.getSpeed());
			
	
		
	}
	
	
	


}