package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import utilities.MatrixMethod;

class MatrixMethodTest {

	@Test
	 public void testMatrixMethod() {
		int[][]x= {{1,2,3},{4,5,6},{5,7,8},{1,2,3}};
		int [][]y= {{1,2,3,1,2,3},{4,5,6,4,5,6},{5,7,8,5,7,8},{1,2,3,1,2,3}};
		assertArrayEquals(y,MatrixMethod.duplicate(x));
	}

}
